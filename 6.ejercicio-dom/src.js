'use strict'
const ps = document.querySelectorAll('p')


for (const p of ps) {
    const palabras = p.textContent.split(' ')
    const palabrasMas5 = palabras.filter((palabra) =>{
        if(palabra.length >= 5) return palabra 
    })
    console.log(palabrasMas5 )
    console.log(palabras)
    p.textContent = palabrasMas5.join(' ')
    p.style.textDecorationStyle = 'solid' 
    p.style.textDecorationColor = 'red'
    p.style.textDecorationLine = 'underline'
}
