'use strict'
const horas = document.querySelector('h1');
const bInicio = document.querySelector('#inicio');
const bPausa = document.querySelector('#pausa');
const bReset = document.querySelector('#reset');

let h = 0;
let m = 0;
let s = 0;

let crono = `${h}:${m}:${s}`;
horas.textContent = crono;

function iniciar(e) {
    const bInicio = e.target;
    const iniciarCrono = setInterval(hms,1000);
};  
function pausar(e) {
    const bPausa = e.target;
    return clearInterval (iniciarCrono);
} ;
function resetear(e ) {
    const bReset = e.target;
    h = 0;
    m = 0;
    s = 0;
    return horas.textContent = `${h}:${m}:${s}`;
};
function hms(){
    s = s+1;
    if (s >= 60) {
        m ++;
        s = 0;
    }
    if (m >= 60) {
        h ++;
        m = 0;
    }   
    crono = `${h}:${m}:${s}`;
    return  horas.textContent = crono;
};

bInicio.addEventListener('click', iniciar);  
bPausa.removeEventListener('click', iniciar);
bReset.addEventListener('click', resetear);
