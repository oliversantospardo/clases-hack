'use strict';

const url = "https://rickandmortyapi.com/api/";

async function getData(url){
    const fetchresponse = await fetch(url);
    let json = await fetchresponse.json();
    

    async function namesCharacters(){
        /* conseguimos los episodios emitidos en enero  */
        const urlEpisodes = json.episodes
        const episodes = await fetch(urlEpisodes)
        json = await episodes.json()
        const arrayEpisodes = json.results
        const arrayCharactersUrls = [] 
        const arrayNamesCharacters = []
        for (let i = 0; i < arrayEpisodes.length; i++) {
            if (arrayEpisodes[i].air_date.slice(0,7) === 'January') {
                const character = arrayEpisodes[i].characters
                arrayCharactersUrls.push(character)
            }
        }
        /* conseguimos los nombres de los personajes de esos capitulos */
        for (const array of arrayCharactersUrls) {
            for (const pos of array) {
                const fetchPos = await fetch(pos)
                json = await fetchPos.json()
                arrayNamesCharacters.push(json.name) 
            }
        }
        return arrayNamesCharacters  
    }
    
    return namesCharacters()

}
/* llamamos la funcion y filtramos los nombres repetidos */
getData(url).then((response) => console.log(filterRepeats(response)))

function filterRepeats (array){
    const filter = array.filter((name, i) => { 
        return array.indexOf(name) === i;
        }
    )
    return filter
};