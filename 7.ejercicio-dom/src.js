'use strict'
/* helpers */
const r = (max) =>  Math.floor(Math.random() * max);

function colorBackground(td){
    const color = `rgb(${r(255)}, ${r(255)}, ${r(255)})`;
    td.style.backgroundColor = color;
}

/* estilo body */
const body = document.querySelector('body');
body.style.display = 'flex'; 
body.style.justifyContent = 'center';
body.style.alignItems = 'center'; 
body.style.background = 'black';
body.style.minHeight = '100vh';


let numInit1 = 4;
let numInit2 = 4;

/* button */
const button = document.createElement ('button');
button.textContent = 'Añadir cuadrado';
body.style.minHeight = '50px';
body.style.minWidth = '100px';
body.append(button);

function handleButtonClick(e) {
    const button = e.target;
    const td = document.createElement('td');
    td.style.minWidth = '200px';
    td.style.minHeight = '200px';
    setInterval(colorBackground,1000,td)
    body.append(td)
}

button.addEventListener('click', handleButtonClick);


/* la tabla */
function createTable(rows,columns){

    const table = document.createElement ('table');
    table.style.minHeight = '50vh';
    table.style.maxWidth = '50vh';

    for (let i = 0; i < rows; i++) {
        const tr = document.createElement ('tr');

        for (let j = 0; j < columns; j++) {
            const td = document.createElement ('td');
            td.style.minWidth = '200px';
            td.style.minHeight = '200px';
            td.style.backgroundColor = `rgb(${r(255)}, ${r(255)}, ${r(255)})`
            setInterval(colorBackground,1000,td);
            tr.append(td);
        };

        table.append(tr);
    };

    body.append(table);
}

createTable(numInit1,numInit2);

