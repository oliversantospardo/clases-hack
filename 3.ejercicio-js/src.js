'use strict'

function conversorBinDec(num,base) {
    /* errores segun los valores introducidos */
    if (isNaN(num) ) {
        alert('numero introducido o valido')
    }else if(base !== '2' && base !== '10'){
        alert('base introducida no valida')
        /* conversion decimal a binario */
    }else if (base === '2') {
        let acum = num;
        const numeroReves = []
        for (let i = 1; i < acum; i) {
            const digito = acum%2;
            acum = Math.floor (acum/2);
            numeroReves.push(digito);
        }
        numeroReves.push(acum);
        const numBinario = numeroReves.reverse().join("");
        alert (`${numBinario}`);
        return numBinario;
    } else {
        /* convertir numero binario a decimal */
        const numeroReves2 = num.split('').reverse();
        let nuevoNumero = [];
        if (numeroReves2.some((numero)=> Number(numero) !== 1   && Number(numero) !== 0)) {
            alert('ese numero no esta en binario')
        } else {
            for (let i = 0; i < numeroReves2.length; i++) {
            const numeroPosicion = numeroReves2[i] * 2 ** i;
            nuevoNumero.push(numeroPosicion);
        }
        let numeroDecimal = 0;
        for (const numero of nuevoNumero) {
            const suma = numeroDecimal + numero;
            numeroDecimal = suma;
        }
        alert(`${numeroDecimal}`)
        return numeroDecimal;
        }

    }
};

console.log(conversorBinDec(prompt('¿Que numero quieres convertir?'),prompt('¿A que base("2","10")?')));