'use strict'

const body = document.querySelector ('body');

async function getList (num) {

    if (isNaN(num)){alert `Eso no es un numero`
    }else {
        const url = `https://randomuser.me/api/?results=${num}`;
        const fetchResponse = await fetch(url); 
        const json = await fetchResponse.json();
        const list = json.results; 
        let listUsers = [];
        for (const user of list) { 
            const name = user.name;
            const gender = user.gender;
            const location = user.location.city;
            const email = user.email;
            const photo = user.picture;
            const userOk = {name, gender,location,email,photo};
            const photoMedium = photo.medium;

            const section = document.createElement ('section');
            section.style.display = 'flex';
            section.style.flexDirection = 'column';
            section.style.justifyContent = 'center';
            section.style.alignItems = 'center';

            const pname = document.createElement ('p');
            const pgender = document.createElement ('p');
            const plocation = document.createElement ('p');
            const pemail = document.createElement ('p');
            const img = document.createElement ('img');
            
            
            pname.textContent = `Nombre: ${name.title} ${name.first} ${name.last}`;
            pgender.textContent = `Genero: ${gender}`;
            plocation.textContent = `Pais: ${location}`;
            pemail.textContent = `Correo electronico: ${email}`;
            img.src = `${photoMedium}`;


            section.append(pname,pgender,plocation,pemail,img);
            body.append(section);
            listUsers.push(userOk);
        }
    return console.log(listUsers);
    }
}

const num = prompt("¿Cuantos usuarios quieres?");

getList(num);
